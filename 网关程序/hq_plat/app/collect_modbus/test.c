
#include <msg_queue_peer.h> //点对点通信
#include <pthread.h>        //开线程
#include <shmem.h>          //共享内存
#include <list.h>           //链表
#include <cJSON.h>          //json
//open
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

union val_t {
    int b_val;   //bool
    int i_val;   //int
    float f_val; //float
};
struct std_node
{
    int key;
    int type;
    int dev_type;
    union val_t old_val;
    union val_t new_val;
    int ret;
};
struct mb_node
{
    int key;        //唯一键
    char name[128]; //数据点名
    int type;       //数据点类
    int addr;       //key对应的数据点地址
};
struct mb_node_list
{
    struct mb_node node;
    struct list_head list;
};
struct msgbuf
{
    long mtype;
    char mdata[256];
};
//共享内存共用结构体
static struct shm_param para;

//数据发送和接收的结构体定义
static struct msgbuf recv_buf;
static struct msgbuf send_buf;
//modbus初始化
unsigned char send_buf1_socket[128] = {0x02, 0xb7, 0x00, 0x00, 0x00, 0x06, 0x01, 0x04, 0x00, 0x00, 0x00, 0x02};
unsigned char send_buf2_socket[128] = {0x02, 0xb7, 0x00, 0x00, 0x00, 0x06, 0x01, 0x04, 0x00, 0x02, 0x00, 0x02};
unsigned char send_buf3_socket[128] = {0x02, 0xb7, 0x00, 0x00, 0x00, 0x06, 0x01, 0x01, 0x00, 0x00, 0x00, 0x01};
unsigned char send_buf4_socket[128] = {0x02, 0xb7, 0x00, 0x00, 0x00, 0x06, 0x01, 0x03, 0x00, 0x00, 0x00, 0x02};
unsigned char send_buf5_socket[128] = {0x02, 0xb7, 0x00, 0x00, 0x00, 0x06, 0x01, 0x01, 0x00, 0x01, 0x00, 0x01};
int sockfd;
int addrlen = sizeof(struct sockaddr);
struct sockaddr_in serveraddr;
//定义内核链表结构体
static struct mb_node_list head;
// static struct list_head *pos;
// static struct mb_node_list *tmp;

//点对点接收线程控制指令
void *mythreadrecv(void *arg)
{
    while (1)
    {

        if (msg_queue_recv("modbus", &recv_buf, sizeof(recv_buf), 0, 0) > 0)
        {
            printf("recv from msgb type = %ld\n", recv_buf.mtype);
            printf("recv from msgb data = %s\n", recv_buf.mdata);
        }
        else
        {
            perror("recv error666:");
            break;
        }
        //接收到的消息进行反序列化
        cJSON *root2 = NULL;
        cJSON *item2 = NULL;
        root2 = cJSON_Parse(recv_buf.mdata);
        int key_modbus = 0;
        if (!root2)
        {
            printf("Error before: [%s]\n", cJSON_GetErrorPtr());
        }
        else
        {
            item2 = cJSON_GetObjectItem(root2, "data");
            key_modbus = (cJSON_GetObjectItem(item2, "key")->valueint);
        }
        //发送命令缓冲区
        //接收命令缓冲区
        unsigned char recv_buf_socket[128] = {0};
        if (key_modbus == 101)
        {
            //温度
            send(sockfd, send_buf1_socket, 12, 0);
        }
        else if (key_modbus == 102)
        {
            //湿度
            send(sockfd, send_buf2_socket, 12, 0);
        }
        else if (key_modbus == 103)
        {
            //空调开关
            send(sockfd, send_buf3_socket, 12, 0);
        }
        else if (key_modbus == 104)
        {
            //空调温度
            send(sockfd, send_buf4_socket, 12, 0);
        }
        else if (key_modbus == 105)
        {
            //灯控
            send(sockfd, send_buf5_socket, 12, 0);
        }
        int ret = recv(sockfd, recv_buf_socket, 128, 0);
        if (ret < 0)
        {
            perror("recv err888");
            continue;
        }
        int i;
        for (i = 0; i < ret; i++)
        {
            //将接收的内容打印出来看看
            printf("%02x ", recv_buf_socket[i]);
        }
        printf("\n");
        //解析出需要的数据
        float *q = recv_buf_socket + 9; //将指针偏移到数据位置
        //小端存储
        float data = *q;
        printf("temp = %0.2f\n", data);
        sleep(1);
        //发送消息
        //定义发送的消息序列化
        cJSON *root3 = cJSON_CreateObject();
        //发送给数据上报进程的消息序列化
        cJSON_AddItemToObject(root3, "type", cJSON_CreateNumber(2));
        cJSON_AddItemToObject(root3, "result", cJSON_CreateNumber(0));
        cJSON_AddItemToObject(root3, "msg", cJSON_CreateString("控制成功"));
        printf("%s\n", cJSON_Print(root3));
        printf("control\n");
        strcpy(send_buf.mdata, cJSON_Print(root3));
        send_buf.mtype = 2;
        if (msg_queue_send("msg_report", &send_buf, sizeof(send_buf), 0) < 0)
        {
            printf("msg_queue_send error\n");
        }
    }
    pause();
    //5关闭链接-close
    close(sockfd);
    pthread_exit(NULL);
}
//共享内存上传发送
void *mythreadsend(void *arg)
{
    //共享内存
    static int shm_ret = -1;
    shm_ret = shm_init(&para, "shm_equipment", 1024);
    if (shm_ret < 0)
    {
        exit(-1);
    }
    struct std_node *node = shm_getaddr(&para);
    if (node == NULL)
    {
        exit(-1);
    }
    unsigned char recv_buf_socket[128] = {0};
   // node->new_val.i_val = 666;
   while(1){
   for(int i=0;i<3;i++){
          if(i==0)
            {
                send(sockfd, send_buf1_socket, 12, 0);
                int ret = recv(sockfd, recv_buf_socket, 128, 0);
                float *q = recv_buf_socket + 9; //将指针偏移到数据位置
                float data = *q;
                printf("temp = %0.2f\n", data);
                node[0].key = 101;
                node[0].new_val.f_val = data;
            }
            else if(i==1)
            {
                send(sockfd, send_buf2_socket, 12, 0);
                int ret = recv(sockfd, recv_buf_socket, 128, 0);
                float *q = recv_buf_socket + 9; //将指针偏移到数据位置
                float data = *q;
                printf("hum = %0.2f\n", data);
                node[0].key = 102;
                node[0].new_val.f_val = data;
                
            }
            else if(i==2)
            {
            }
           // sleep(1);
   }
        }
    pthread_exit(NULL);
}

int main(int argc, char *argv[])
{
    INIT_LIST_HEAD(&head.list); //初始化链接
    //1创建套接字--socket
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
        perror("socket err");
        exit(-1);
    }
    //2ָ指定服务器地址--sockaddr_in
    bzero(&serveraddr, addrlen);
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_addr.s_addr = inet_addr("192.168.31.212");
    serveraddr.sin_port = htons(502);
    //3链接服务器--connect
    if (connect(sockfd, (struct sockaddr *)&serveraddr, addrlen) < 0)
    {
        perror("connect err");
        exit(-1);
    }
    //反序列化
    char buf[1090];
    cJSON *root = NULL;
    cJSON *item = NULL;
    cJSON *array = NULL;
    cJSON *temp = NULL;
    //打开json文件
    int fd = open("./node.json", O_RDONLY);
    if (fd == -1)
    {
        perror("open err.");
        return -1;
    }
    size_t ret = read(fd, buf, 1090);
    if (ret < 0)
    {
        perror("read err.");
        return -1;
    }
    printf("***************反序列**********************************\n");
    root = cJSON_Parse(buf);
    if (!root)
    {
        printf("Error before: [%s]\n", cJSON_GetErrorPtr());
    }
    else
    {
        item = cJSON_GetObjectItem(root, "modbus");
        printf("%s\n", cJSON_Print(item));
        item = cJSON_GetObjectItem(root, "modbus");
        array = cJSON_GetObjectItem(item, "data");
        //添加链表
        for (int i = 0; i < cJSON_GetArraySize(array); i++)
        {
            temp = cJSON_GetArrayItem(array, i);
            //使用循环为JSON中的数组中的值进行赋�?
            struct mb_node_list *mb = (struct mb_node_list *)malloc(sizeof(struct mb_node_list));
            mb->node.key = cJSON_GetObjectItem(temp, "key")->valueint;
            strcpy(mb->node.name, cJSON_Print(cJSON_GetObjectItem(temp, "name")));
            mb->node.addr = cJSON_GetObjectItem(temp, "addr")->valueint;
            mb->node.type = cJSON_GetObjectItem(temp, "type")->valueint;
            list_add_tail(&mb->list, &head.list);
        }
    }
    printf("***********************发送和接收消息*******************\n");
    //线程id
    pthread_t tid_send;
    pthread_t tid_recv;
    if (pthread_create(&tid_send, NULL, mythreadsend, NULL) != 0)
    {
        perror("pthread_create1 err.");
        return -1;
    }
    if (pthread_create(&tid_recv, NULL, mythreadrecv, NULL) != 0)
    {
        perror("pthread_create1 err.");
        return -1;
    }
    pthread_join(tid_send, NULL);
    pthread_join(tid_recv, NULL);
    return 0;
}
