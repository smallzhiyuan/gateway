#ifndef M0__H
#define M0__H

#include <termios.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdint.h>
#include <shmem.h>

union val_t {
        int b_val;	 //bool
        int i_val;	 //int
        float f_val; //float
};
struct std_node
{
        int key;
        int type;
        int dev_type;
        union val_t old_val;
        union val_t new_val;
        int ret;
};


struct env_info {
    uint8_t head[3];	 //标识位st:
    uint8_t type;		 //数据类型
    uint8_t snum;		 //房间编号
    uint8_t temp[2];	 //温度	
    uint8_t hum[2];		 //湿度
    uint8_t x;			 //三轴信息
    uint8_t y;			 
    uint8_t z;			 
    uint32_t ill;		 //光照
    uint32_t bet;		 //电池电量
    uint32_t adc; 		 //电位器信息
};


#define ENV_LEN sizeof(struct env_info)
#define CONVER_ENV_LEN sizeof(struct conver_env_info)

float temperature;	 //温度	
float humidity;		 //湿度
int ill;		 //光照
    
float dota_atof (char unitl);
int dota_atoi (const char *cDecade);
float dota_adc (unsigned int ratio);
int m0_dev(void);
void serial_init(int fd);

#define DEV_UART 			"/dev/ttyUSB0"

#endif