#include <stdio.h>
#include <stdlib.h>
#include "list.h"
#include "cJSON.h"
#include <pthread.h>
#include "msg_queue_peer.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "m0.h"

struct m0_node
{
	int type;		//数据点类型
	int key;		//唯一键值
	char name[128]; //数据点名称
};

struct msgbuf
{
	long type;
	char mdata[256];
};

struct m0_node_list
{
	struct m0_node node;
	struct list_head list;
};

struct msgbuf send_buf;
struct msgbuf recv_buf;
static struct shm_param m0_para;
static struct m0_node_list head;
struct list_head *pos;

unsigned char LED_ON = 0xA1; //灯
unsigned char LED_OFF = 0xA0;
unsigned char ALARM_ON = 0x91; //蜂鸣器
unsigned char ALARM_OFF = 0x90;
unsigned char FAN_ON1 = 0x81; //风扇
unsigned char FAN_ON2 = 0x82;
unsigned char FAN_ON3 = 0x83;
unsigned char FAN_OFF = 0x80;

int fd; //数据上传

void *mypthreadIn(void *arg);
void m0_review();

int main()
{
	cJSON *root = NULL;
	cJSON *item = NULL; //cjson对象
	cJSON *array = NULL;
	cJSON *temp = NULL;
	char buf[2048] = {0};

	INIT_LIST_HEAD(&head.list);

	int fp = open("./node.json", O_RDWR); //打开json点表
	if (fp == -1)
	{
		perror("open error");
		return -1;
	}
	size_t ret = read(fp, buf, 1090);
	if (ret < 0)
	{
		perror("read err.");
		return -1;
	}
	root = cJSON_Parse(buf);
	if (!root)
	{
		printf("Error before\n");
	}
	else
	{
		item = cJSON_GetObjectItem(root, "m0");
		//添加链表
		array = cJSON_GetObjectItem(item, "data");
		for (int i = 0; i < cJSON_GetArraySize(array); i++)
		{
			temp = cJSON_GetArrayItem(array, i);
			//使用循环为JSON中的数组中的值进行赋值
			struct m0_node_list *m = (struct m0_node_list *)malloc(sizeof(struct m0_node_list));
			m->node.key = cJSON_GetObjectItem(temp, "key")->valueint;
			strcpy(m->node.name, cJSON_Print(cJSON_GetObjectItem(temp, "name")));
			m->node.type = cJSON_GetObjectItem(temp, "type")->valueint;
			list_add_tail(&m->list, &head.list);
		}
	}
	int nbytes = 0;
	struct env_info envinfo; //原始数据

	static int shm_ret = -1;
	//共享内存的创建获取
	shm_ret = shm_init(&m0_para, "shm_m0", 1024);
	if (shm_ret < 0)
	{
		return -1;
	}
	struct std_node *node = shm_getaddr(&m0_para);
	if (node == NULL)
	{
		return -1;
	}
	//温湿度和光照的实时检测
	if ((fd = open(DEV_UART, O_RDWR)) < 0)
	{
		perror("open uart err\n");
		return -1;
	}
	serial_init(fd);
	//线程
	pthread_t tidin;
	if (pthread_create(&tidin, NULL, mypthreadIn, NULL) != 0)
	{
		perror("pthread_create err.");
		return -1;
	}
	while (1)
	{
		nbytes = read(fd, &envinfo, ENV_LEN);
		if (nbytes == ENV_LEN)
		{
			// printf("raw data ill=%d, x=%d, y=%d, z=%d\n",
			// 	   envinfo.ill, envinfo.x, envinfo.y, envinfo.z);
		}
		else
		{
			printf("err data");
		}
		temperature = envinfo.temp[0] + dota_atof(envinfo.temp[1]);
		humidity = envinfo.hum[0] + dota_atof(envinfo.hum[1]);
		ill = envinfo.ill;

		// printf("conver temperature=%0.2f, humidity=%0.2f, ill=%d\n",
		// 	   temperature, humidity, ill);

		for (int i = 0; i < 3; i++)
		{ //共享内存实时上传数据
			if (i == 0)
			{
				node[0].key = 1;
				node[0].new_val.f_val = temperature;
			}
			else if (i == 1)
			{
				node[1].key = 2;
				node[1].new_val.f_val = humidity;
			}
			else if (i == 2)
			{
				node[2].key = 3;
				node[2].new_val.i_val = ill;
			}
		}
	}

	pthread_join(tidin, NULL);
	return 0;
}

void *mypthreadIn(void *arg)
{
	cJSON *root2 = NULL;
	cJSON *item2 = NULL;
	int key;
	union val_t val;
	while (1)
	{

		if (msg_queue_recv("m0", &recv_buf, sizeof(recv_buf), 0, 0) > 0)
		{
			root2 = cJSON_Parse(recv_buf.mdata);
		}
		else
		{
			perror("recv error.");
			break;
		}
		item2 = cJSON_GetObjectItem(root2, "data");
		key = atoi(cJSON_Print(cJSON_GetObjectItem(item2, "key")));
		printf("m0key:%d\n", key);
		val.i_val = atoi(cJSON_GetObjectItem(item2, "val")->valuestring);
		printf("val:%d\n", val.i_val);
		switch (key)
		{
		case 1:
			m0_review();
			break;
		case 2:
			m0_review();
			break;
		case 3:
			m0_review();
			break;
		case 4:
			if (val.i_val == 0)
			{
				write(fd, &LED_OFF, 1);
				m0_review();
			}
			else
			{
				write(fd, &LED_ON, 1);
				m0_review();
			}
			break;
		case 5:
			if (val.i_val == 0)
			{
				write(fd, &ALARM_OFF, 1);
				m0_review();
			}
			else
			{
				write(fd, &ALARM_ON, 1);
				m0_review();
			}
			break;
		case 6:
			if (val.i_val == 0)
			{
				write(fd, &FAN_OFF, 1);
				m0_review();
			}
			else if (val.i_val == 1)
			{
				write(fd, &FAN_ON1, 1);
				m0_review();
			}
			else if (val.i_val == 2)
			{
				write(fd, &FAN_ON2, 1);
				m0_review();
			}
			else if (val.i_val == 3)
			{
				write(fd, &FAN_ON3, 1);
				m0_review();
			}
			break;
		default:
		{
			printf("recv error\n");
			break;
		}
		}
	}
	pause();
	pthread_exit(NULL);
}
void m0_review()
{
	cJSON *newroot = cJSON_CreateObject();
	cJSON_AddItemToObject(newroot, "type", cJSON_CreateNumber(2));
	cJSON_AddItemToObject(newroot, "result", cJSON_CreateNumber(0));
	cJSON_AddItemToObject(newroot, "msg", cJSON_CreateString("控制成功"));
	strcpy(send_buf.mdata, cJSON_Print(newroot));
	send_buf.type=1;
	if (msg_queue_send("msg_report", &send_buf, sizeof(send_buf), 0) < 0)
	{
		printf("send error\n");
	}
}