#ifndef HEAD__H
#define HEAD__H

#include <cJSON.h>
#include <MQTTClient.h>
#include <msg_queue_peer.h>
#include <string.h>
#include <shmem.h>

#define TOPIC       "/app/data/down"           //接收客户端节点查询
#define TOPICPUB    "/app/data/up"             //上报节点信息
#define TOPICCONSUB "/app/control/down"        //接收客户端控制命令
#define TOPICCONPUB "/app/control/up"          //回复控制消息
#define TOPICMODSUB "/app/mode/down"           //接收客户端修改协议命令
#define TOPICMODPUB "/app/mode/up"             //回复修改协议消息
#define QOS         1
#define TIMEOUT     10000L
#define M0  0                                  //设备对应关系
#define MODBUS 1

union val_t
{ 
	int b_val;  //bool类型存储空间
	int i_val;   //整形值存储空间
	float f_val;  //浮点值存储空间
};
//共享内存结构体   上报进程 <----> m0 & modbus
struct std_node
{
	int key;  //唯一键值
	int type;  //数据点类型
	int dev_type;  //属于哪个设备：m0或modbus
	union val_t old_val;  //变化上报后需要更新旧值
	union val_t new_val;  //从共享内存取出最新数据，放到new_val中
	int ret;  //默认为-1，采集成功后设置为0，采集失败再置-1
};

//测试结构体
struct msgbuf
{
	long mtype;
	char mdata[256];
};

int parse_jsontable(void);//解析点表函数
char* data_tojson(char *tem,char *hum,char *ill);//m0采集数据转为json-->>QT
char* modbusdata_tojson(char *tem,char *hum,char *ill);//采集数据转为json-->>QT
char* mode_tojson(void);
//将共享内存json 序列化后的数据发送给QT client
void senddata_toclient(char *json,int flag,MQTTClient client,MQTTClient_connectOptions conn_opts);
void do_selectdata(char *topicName,MQTTClient_message *message,
                  MQTTClient client,MQTTClient_connectOptions conn_opts);//查询节点功能
int do_control(char *topicName,MQTTClient_message *message,
                MQTTClient client,MQTTClient_connectOptions conn_opts);//控制功能
void do_changemod(char *topicName,MQTTClient_message *message,
                 MQTTClient client,MQTTClient_connectOptions conn_opts);//修改功能			 
#endif


