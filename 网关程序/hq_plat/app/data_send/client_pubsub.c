
/*******************************************************************************
 * Copyright (c) 2022
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v1.0 which accompany this distribution. 
 *
 * Contributors:
 *    Ian Craggs - initial contribution
 *******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "head.h"

#define ADDRESS "tcp://192.168.31.242:1883"
#define CLIENTID "ExampleClientSub"

//mqtt参数
MQTTClient client;
MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;
//数据上报模式
extern int data_mode;	//上报模式
extern int data_period; //周期上报周期
extern char temperature[32];
extern char humidity[32];
extern char ill[32];
extern char mb_temperature[32];
extern char mb_humidity[32];
extern char mb_ill[32];

static struct shm_param m0_para;
static struct shm_param modbus_para;
struct std_node *node;
struct std_node *node_modbus;

volatile MQTTClient_deliveryToken deliveredtoken;
void delivered(void *context, MQTTClient_deliveryToken dt)
{
	deliveredtoken = dt;
}
//3.上报进程接收消息
int msgarrvd(void *context, char *topicName, int topicLen, MQTTClient_message *message)
{
	//1 接收客户端节点查询 订阅主题
	if (strcmp(topicName, TOPIC) == 0)
	{
		//4.1 节点数据查询(上报进程发布消息>>QT)
		do_selectdata(topicName, message, client, conn_opts);
		
	}
	else if (strcmp(topicName, TOPICCONSUB) == 0)
	{
		//4.2 控制(上报进程发布消息>>QT)
		do_control(topicName, message, client, conn_opts);
	}
	else if (strcmp(topicName, TOPICMODSUB) == 0)
	{
		//4.3 修改(上报进程发布消息>>QT)
		do_changemod(topicName, message, client, conn_opts);
	}
	MQTTClient_freeMessage(&message);
	MQTTClient_free(topicName);
	return 1;
}
void connlost(void *context, char *cause)
{
	printf("\nConnection lost\n");
	printf("     cause: %s\n", cause);
}
//数据上报线程
void *mythread_senddata(void *arg)
{
	while (1)
	{
		for (int i = 0; i < 3; i++)
		{
			if (node[i].key == 1)
			{
				sprintf(temperature, "%.2f", node[0].new_val.f_val);
				
			}
			else if (node[i].key == 2)
			{
				sprintf(humidity, "%.2f", node[1].new_val.f_val);
			}
			else if (node[i].key == 3)
			{
				sprintf(ill, "%d", node[2].new_val.i_val);
			}

			if (node_modbus[i].key == 101)
			{
				sprintf(mb_temperature, "%.2f", node_modbus[i].new_val.f_val);
			}
			else if (node_modbus[i].key == 102)
			{
				sprintf(mb_humidity, "%.2f", node_modbus[i].new_val.f_val);
			}
		}
	
	    if (data_mode == 1)
		{ 
			if (node[0].new_val.f_val != node[0].old_val.f_val)
			senddata_toclient(data_tojson(temperature, humidity, ill), 0, client, conn_opts);
			if (node[1].new_val.f_val != node[1].old_val.f_val)
			senddata_toclient(data_tojson(temperature, humidity, ill), 0, client, conn_opts);
			if (node[2].new_val.i_val != node[2].old_val.i_val)
			senddata_toclient(data_tojson(temperature, humidity, ill), 0, client, conn_opts);
			sleep(1);
		}
		else if (data_mode == 2)
		{
			senddata_toclient(data_tojson(temperature, humidity, ill), 0, client, conn_opts);
			senddata_toclient(modbusdata_tojson(mb_temperature, mb_humidity, "0"), 0, client, conn_opts);
			printf("周期 period=%d\n", data_period);
			sleep(data_period);
		}
       node[0].old_val.f_val = node[0].new_val.f_val;
	   node[1].old_val.f_val = node[1].new_val.f_val;
       node[2].old_val.i_val = node[2].new_val.i_val;	
	}
	pause();
	pthread_exit(NULL);
}
int main(int argc, char *argv[])
{
	int rc;
	char ch;
	//1.解析json   size json :1084
	parse_jsontable(); //解析点表
	//2.连接mqtt
	MQTTClient_create(&client, ADDRESS, CLIENTID,
					  MQTTCLIENT_PERSISTENCE_NONE, NULL);
	conn_opts.keepAliveInterval = 20;
	conn_opts.cleansession = 1;
	MQTTClient_setCallbacks(client, NULL, connlost, msgarrvd, delivered); //设置订阅回调函数
	if ((rc = MQTTClient_connect(client, &conn_opts)) != MQTTCLIENT_SUCCESS)
	{
		printf("Failed to connect, return code %d\n", rc);
		exit(EXIT_FAILURE);
	}
	printf("client %s using QoS%d\n"
		   "connect mqtt Press Q<Enter> to quit\n\n",
		   CLIENTID, QOS);
	MQTTClient_subscribe(client, TOPIC, QOS);
	MQTTClient_subscribe(client, TOPICCONSUB, QOS);
	MQTTClient_subscribe(client, TOPICMODSUB, QOS);
	// 共享内存>>m0
	static int shm_ret = -1;
	shm_ret = shm_init(&m0_para, "shm_m0", 1024);
	if (shm_ret < 0)
	{
		return -1;
	}
	node = shm_getaddr(&m0_para);
	if (node == NULL)
	{
		return -1;
	}
	// 共享内存>>modbus
	shm_ret = shm_init(&modbus_para, "shm_equipment", 1024);
	if (shm_ret < 0)
	{
		return -1;
	}
	node_modbus = shm_getaddr(&modbus_para);
	if (node_modbus == NULL)
	{
		return -1;
	}
	//开线程
	pthread_t tid_senddata;
	if (pthread_create(&tid_senddata, NULL, mythread_senddata, NULL) != 0)
	{
		perror("pthread_create1 err.");
		return -1;
	}
	pthread_detach(tid_senddata);
	do
	{
		ch = getchar();
	} while (ch != 'Q' && ch != 'q');
	MQTTClient_unsubscribe(client, TOPIC);
	MQTTClient_unsubscribe(client, TOPICCONSUB);
	MQTTClient_unsubscribe(client, TOPICMODSUB);
	MQTTClient_unsubscribe(client, TOPICPUB);
	MQTTClient_disconnect(client, 10000);
	MQTTClient_destroy(&client);
	return rc;
}
