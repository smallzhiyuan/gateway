#include "head.h"

int data_mode;
int data_period;//周期上报周期
char json_buf[1088]={0};

 char temperature[32];
 char humidity[32];
 char ill[32];
 char mb_temperature[32];
 char mb_humidity[32];
 char mb_ill[32];

//解析点表
int parse_jsontable(void){
	cJSON *root = NULL;
	cJSON *item = NULL; //cjson对象
	cJSON *item_next= NULL;
	FILE *fp = fopen("./node.json", "r");
	if (fp == NULL)
	{
		perror("fopen error");
		return -1;
	}
	int fd=fread(json_buf, sizeof(json_buf), 1, fp);
	UNUSED(fd);
	printf("size %d\n",strlen(json_buf));
	root = cJSON_Parse(json_buf);
	if (!root)
	{
		printf("Error before\n");
	}
	else
	{
		//带格式打印json
		// printf("%s\n\n",cJSON_Print(root));
		item = cJSON_GetObjectItem(root, "version");
		item = cJSON_GetObjectItem(root, "mqtt_server");
		item_next = cJSON_GetObjectItem(item, "addr");
		item_next = cJSON_GetObjectItem(item, "port");
		item = cJSON_GetObjectItem(root, "report");
		item_next = cJSON_GetObjectItem(item, "type");
		data_mode = atoi(cJSON_Print(item_next));
		item_next = cJSON_GetObjectItem(item, "period");
		data_period = atoi(cJSON_Print(item_next));	
	}
	return 1;
}


//初版  节点数据初始序列化
char* data_tojson(char *tem,char *hum,char *ill){
	cJSON *root = NULL;
	root = cJSON_CreateObject();
	cJSON *array = cJSON_CreateArray();    //array;
	cJSON *array_1 = cJSON_CreateObject(); //数组成员
	cJSON *array_2 = cJSON_CreateObject();
	cJSON *array_3 = cJSON_CreateObject();

	cJSON_AddItemToObject(root, "type", cJSON_CreateNumber(1));
	cJSON_AddItemToObject(root, "result",cJSON_CreateNumber(0));
	cJSON_AddItemToObject(root, "data", array);
	cJSON_AddItemToObject(array_1, "key", cJSON_CreateNumber(1));
	cJSON_AddItemToObject(array_1, "val", cJSON_CreateString(tem));
	cJSON_AddItemToArray(array, array_1);
	cJSON_AddItemToObject(array_2, "key", cJSON_CreateNumber(2));
	cJSON_AddItemToObject(array_2, "val", cJSON_CreateString(hum));
	cJSON_AddItemToArray(array, array_2);
	cJSON_AddItemToObject(array_3, "key", cJSON_CreateNumber(3));
	cJSON_AddItemToObject(array_3, "val", cJSON_CreateString(ill));
	cJSON_AddItemToArray(array, array_3);
	return cJSON_Print(root);
}

char* modbusdata_tojson(char *tem,char *hum,char *ill){
    cJSON *root = NULL;
	root = cJSON_CreateObject();
	cJSON *array = cJSON_CreateArray();    //array;
	cJSON *array_1 = cJSON_CreateObject(); //数组成员
	cJSON *array_2 = cJSON_CreateObject();
	cJSON *array_3 = cJSON_CreateObject();

	cJSON_AddItemToObject(root, "type", cJSON_CreateNumber(1));
	cJSON_AddItemToObject(root, "result",cJSON_CreateNumber(0));
	cJSON_AddItemToObject(root, "data", array);
	cJSON_AddItemToObject(array_1, "key", cJSON_CreateNumber(101));
	cJSON_AddItemToObject(array_1, "val", cJSON_CreateString(tem));
	cJSON_AddItemToArray(array, array_1);
	cJSON_AddItemToObject(array_2, "key", cJSON_CreateNumber(102));
	cJSON_AddItemToObject(array_2, "val", cJSON_CreateString(hum));
	cJSON_AddItemToArray(array, array_2);
	cJSON_AddItemToObject(array_3, "key", cJSON_CreateNumber(103));
	cJSON_AddItemToObject(array_3, "val", cJSON_CreateString(ill));
	cJSON_AddItemToArray(array, array_3);
	return cJSON_Print(root);

}

//将共享内存json 序列化后的数据发送给QT client  flag: 0上报数据 1回复控制 2回复协议修改
void senddata_toclient(char *json,int flag,MQTTClient client,MQTTClient_connectOptions conn_opts){

	MQTTClient_message pubmsg = MQTTClient_message_initializer;
	MQTTClient_deliveryToken token;
	pubmsg.payload = json;
	pubmsg.payloadlen = (int)strlen(json);
	pubmsg.qos = QOS;
	pubmsg.retained = 0;
	if(flag==0)
	 MQTTClient_publishMessage(client, TOPICPUB, &pubmsg, &token);//上报进程---》客户端  发布消息
	if(flag==1)
	 MQTTClient_publishMessage(client, TOPICCONPUB, &pubmsg, &token);//上报进程---》客户端  发布消息
    if(flag==2)
	 MQTTClient_publishMessage(client, TOPICMODPUB, &pubmsg, &token);//上报进程---》客户端  发布消息
}

//查询节点功能
void do_selectdata(char *topicName,MQTTClient_message *message,MQTTClient client,MQTTClient_connectOptions conn_opts){
	char* payloadptr;
	cJSON *root = NULL;
	cJSON *item = NULL; //cjson对象
	printf("Message arrived\n");
	printf("   topic: %s\n", topicName);
	printf("   message:\n");
	payloadptr = message->payload;
	root = cJSON_Parse(payloadptr);
	if (!root)
	{
		printf("Error before\n");
	}
	else
	{
		printf("%s\n\n",cJSON_Print(root));
		item = cJSON_GetObjectItem(root, "type");
		printf("type:%s\n", cJSON_Print(item));
		item = cJSON_GetObjectItem(root, "limit");
		printf("limit:%s\n", cJSON_Print(item));
		if(data_mode==0){
		senddata_toclient(data_tojson(temperature, humidity, ill), 0, client, conn_opts);
		//senddata_toclient(modbusdata_tojson(temperature, humidity, ill), 0, client, conn_opts);
		}
	}
}
//控制指令功能
int do_control(char *topicName,MQTTClient_message *message,
		MQTTClient client,MQTTClient_connectOptions conn_opts){
	char* payloadptr;
	cJSON *root = NULL;
	cJSON *item = NULL;
	cJSON *item_next = NULL;
	struct msgbuf control_sendbuf; // 接收命令
	struct msgbuf control_recvbuf; // 接收命令
	int type;

	control_sendbuf.mtype = 1;  
	printf("   topic: %s\n", topicName);
	payloadptr = message->payload;
	root = cJSON_Parse(payloadptr);
	if (!root)
	{
		printf("Error before\n");
	}
	else
	{
		item = cJSON_GetObjectItem(root,"data");
		item_next = cJSON_GetObjectItem(item,"key");
		type=atoi(cJSON_Print(item_next));
		printf("type:%d\n",type);
		sprintf(control_sendbuf.mdata,"%s",payloadptr);
		printf("buf11:%s\n",control_sendbuf.mdata);
		//判断是哪个设备
		if(type>0 && type<8){
			//5.上报进程 >> M0 
			if(msg_queue_send("m0", &control_sendbuf, sizeof(control_sendbuf), 0) < 0)
			{
				printf("m0 msg_queue_send error\n");
				return -1;
			}
			if (msg_queue_recv("msg_report", &control_recvbuf, sizeof(control_recvbuf), 0, 0) > 0)
            {
            printf("recv from m0 type = %ld\n", control_recvbuf.mtype);
            printf("recv from m0 data = %s\n", control_recvbuf.mdata);
			senddata_toclient(control_recvbuf.mdata,1,client,conn_opts);
            }
		}else if(type>100 && type<108){
		    //6 上报进程 >> Modbus
			if(msg_queue_send("modbus", &control_sendbuf, sizeof(control_sendbuf), 0) < 0)
			{
				printf("modbus msg_queue_send error\n");
				return -1;
			}
			// if (msg_queue_recv("msg_report", &control_recvbuf, sizeof(control_recvbuf), 0, 0) > 0)
            //{
            // printf("recv from modbus type = %ld\n", control_recvbuf.mtype);
            // printf("recv from modbus data = %s\n", control_recvbuf.mdata);
			//senddata_toclient(control_recvbuf.mdata,1,client,conn_opts);
            // }
		}
		
	}
	return 0;
}

//修改模式功能
void do_changemod(char *topicName,MQTTClient_message *message,
		MQTTClient client,MQTTClient_connectOptions conn_opts){
	char* payloadptr;
	cJSON *root = NULL;
	cJSON *item = NULL;
	cJSON *item_next = NULL;
	printf("   topic: %s\n", topicName);
	payloadptr = message->payload;
	//printf("%s\n",payloadptr);
	root = cJSON_Parse(payloadptr);
	if (!root)
	{
		printf("do_changemod Error before\n");
	}
	else
	{
		printf("%s\n\n",cJSON_Print(root));
		item = cJSON_GetObjectItem(root, "data");
		item_next = cJSON_GetObjectItem(item, "type");
		data_mode =atoi(cJSON_Print(item_next));//获取协议类型 0不上报   1 变化 2 周期
        senddata_toclient(mode_tojson(),2,client,conn_opts);
		if(data_mode==2){
			item_next = cJSON_GetObjectItem(item, "period");
			data_period = atoi(cJSON_Print(item_next));
		}
	}
}
//回复指令
char* mode_tojson(void){
	cJSON *root = cJSON_CreateObject();
    //发送给数据上报进程的消息序列化
    cJSON_AddItemToObject(root, "result", cJSON_CreateNumber(1));
    cJSON_AddItemToObject(root, "msg", cJSON_CreateString("修改成功"));
	return cJSON_Print(root);
}