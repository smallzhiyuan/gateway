/***********************************************************************************
Copy right:	    Coffee Tech.
Author:         jiaoyue
Date:           2019.7.31
Description:    本地套接字通信接口组件
***********************************************************************************/

#ifndef UNIX_SOCK_H
#define UNIX_SOCK_H

#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>
#include <pub_define.h>

#define MAX_UNIX_LEN (208*KB)

#define UNIX_NAME_SZ    16

struct unix_param
{
    int cfd;
    int sfd;
    struct sockaddr_un saddr;
    struct sockaddr_un caddr;
    char cname[UNIX_NAME_SZ+1];    //客户端进程名
    char sname[UNIX_NAME_SZ+1];    //服务端进程名
};

//服务相关
int unix_init_server(struct unix_param *para, const char *sname);
ssize_t unix_server_recv(struct unix_param *para, void *buf, size_t len);
ssize_t unix_server_send(const struct unix_param *para, const void *buf, size_t len);

//客户端相关
int unix_init_client(struct unix_param *para, const char *cname, const char *sname);
ssize_t unix_client_send(const struct unix_param *para, const void *buf, size_t len);
ssize_t unix_client_send_nowait(const struct unix_param *para, const void *buf, size_t len);
ssize_t unix_client_recv(const struct unix_param *para, void *buf, size_t len, struct timeval *timeout);

#endif  // UNIX_SOCK_H
