/***********************************************************************************
Copy right:	    Coffee Tech.
Author:         jiaoyue
Date:           2019-08-06
Description:    msg_queue模块:提供消息队列组件
***********************************************************************************/

#ifndef MSG_QUEUE_H
#define MSG_QUEUE_H

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <pub_define.h>

#define MSG_NAME_SZ    32

struct msg_param
{
    long msg_type;                //命令类型
    key_t ckey;                   //客户端id
    key_t skey;                   //服务端id
    char cname[MSG_NAME_SZ+1];    //客户端进程名
    char sname[MSG_NAME_SZ+1];    //服务端进程名
};

//服务相关
int msg_service_init(struct msg_param *msg, const char *sname);
int msg_service_recv(struct msg_param *msg, void *msg_ptr, size_t msgsz);
int msg_service_send(const struct msg_param *msg, void *msg_ptr, size_t msgsz);

//客户端相关
int msg_client_init(struct msg_param *msg, const char *cname, const char *sname);
int msg_client_send(const struct msg_param *msg, void *msg_ptr, size_t msgsz);
int msg_client_recv(struct msg_param *msg, void *msg_ptr, size_t msgsz);
int msg_client_recv_nowait(struct msg_param *msg, void *msg_ptr, size_t msgsz);

#endif  // MSG_QUEUE_H
