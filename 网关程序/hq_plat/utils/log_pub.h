/***********************************************************************************
Copy right:	    Coffee Tech.
Author:         jiaoyue
Date:           2019.8.5
Description:    log子系统公共头文件剥离
***********************************************************************************/

#ifndef LOG_PUB_H
#define LOG_PUB_H

#include <pub_define.h>

#define MAX_LOG_BUF_LEN (20*KB)  //打印内容不能超过这个

//总打印级别控制
#define	APP_EMERG	"<0>"	/* system is unusable			*/
#define	APP_ALERT	"<1>"	/* action must be taken immediately	*/
#define	APP_CRIT	"<2>"	/* critical conditions			*/
#define	APP_ERR     "<3>"	/* error conditions			*/
#define	APP_WARNING	"<4>"	/* warning conditions			*/
#define	APP_NOTICE	"<5>"	/* normal but significant condition	*/
#define	APP_INFO	"<6>"	/* informational			*/
#define	APP_DEBUG	"<7>"	/* debug-level messages			*/

#define	LOG_STD_FLAG  "std"	//标准输出
#define	LOG_ORG_FLAG  "org"	//原样输出
#define	LOG_CMD_FLAG  "cmd"	//命令处理

#define SERVICE_LOG_NAME "log"

#endif  //LOG_PUB_H
