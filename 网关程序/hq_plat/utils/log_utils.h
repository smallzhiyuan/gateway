/***********************************************************************************
Copy right:	    Coffee Tech.
Author:         jiaoyue
Date:           2019.8.1
Description:    log组件接口
***********************************************************************************/

#ifndef LOG_UTILS_H
#define LOG_UTILS_H

#include <stdarg.h>
#include <time.h>
#include <sys/time.h>
#include <log_pub.h>

void set_dbg_level(const char *limit);  //设置打印级别，取值为APP_INFO等

//这两个不对外开放
int log_std(const char *format, ...);
long get_sys_runtime(int type);

/************************使用下面宏进行打印*****************************/
//只有打印级别起作用，原样打印，无其它信息，无换行
#define LOG(APP_LEVEL, CONTENT, ...)\
    log_std(LOG_ORG_FLAG APP_LEVEL CONTENT, ##__VA_ARGS__)

//带所有有用信息
#define LOG_STD(APP_LEVEL, MODULE_NAME, CONTENT, ...)\
    log_std(LOG_STD_FLAG APP_LEVEL "[%05ld.%03ld]%s[%s][%s][%d]:" CONTENT, \
        get_sys_runtime(1), get_sys_runtime(2), APP_LEVEL, MODULE_NAME, __FILE__, __LINE__, ##__VA_ARGS__)

#define LOGD(CONTENT, ...) \
    LOG_STD(APP_DEBUG, MODULE, CONTENT, ##__VA_ARGS__);

#define LOGI(CONTENT, ...) \
    LOG_STD(APP_INFO, MODULE, CONTENT, ##__VA_ARGS__);

#define LOGN(CONTENT, ...) \
    LOG_STD(APP_NOTICE, MODULE, CONTENT, ##__VA_ARGS__);

#define LOGW(CONTENT, ...) \
    LOG_STD(APP_WARNING, MODULE, CONTENT, ##__VA_ARGS__);

#define LOGE(CONTENT, ...) \
    LOG_STD(APP_ERR, MODULE, CONTENT, ##__VA_ARGS__);

#endif  //LOG_UTILS_H
