/***********************************************************************************
Copy right:     Coffee Tech.
Author:         jiaoyue
Date:           2021.11.23
Description:    共享内存测试
***********************************************************************************/

#include <shmem.h>

static struct shm_param para;

struct student
{
    int num;
    char name[64];
};

int main(int argc, char *argv[])
{
    int ret = -1;

    ret = shm_init(&para, "shm_test", 1024);
    if(ret < 0)
    {
        return -1;
    }

    struct student *addr = shm_getaddr(&para);
    if(addr == NULL)
    {
        return -1;
    }

    addr->num = 10;
    strcpy(addr->name, "zhangsan");

    return 0;
}
