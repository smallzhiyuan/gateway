/***********************************************************************************
Copy right:     Coffee Tech.
Author:         jiaoyue
Date:           2021.11.23
Description:    共享内存测试
***********************************************************************************/

#include <shmem.h>

static struct shm_param para;

struct student
{
    int num;
    char name[64];
};

int main(int argc, char *argv[])
{
    int ret = -1;

    ret = shm_init(&para, "shm_test", 1024);
    if(ret < 0)
    {
        return -1;
    }

    struct student *addr = shm_getaddr(&para);
    if(addr == NULL)
    {
        return -1;
    }

    printf("num = %d\n", addr->num);
    printf("num = %s\n", addr->name);

    shm_del(&para);

    return 0;
}
