/***********************************************************************************
Copy right:     Coffee Tech.
Author:         jiaoyue
Date:           2019.11.21
Description:    测试msg_peer组件
***********************************************************************************/

#include <msg_queue_peer.h>

struct msgbuf
{
    long mtype;
    char mdata[256];
};

int main(int argc, char *argv[])
{
    struct msgbuf send_buf;
    struct msgbuf recv_buf;

    send_buf.mtype = 1;

    while (1)
    {
        if(msg_queue_recv("msga", &recv_buf, sizeof(recv_buf), 0, 0) > 0)
        {
            printf("recv from msga type = %ld\n", recv_buf.mtype);
            printf("recv from msga data = %s\n", recv_buf.mdata);
        }
        else
        {
            perror("recv error:");
            break;
        }

        char *p = fgets(send_buf.mdata, 256, stdin);
        UNUSED(p);
        if(msg_queue_send("msgb", &send_buf, sizeof(send_buf), 0) < 0)
        {
            printf("msg_queue_send error\n");
            return -1;
        }
    }


    pause();

    return 0;
}
