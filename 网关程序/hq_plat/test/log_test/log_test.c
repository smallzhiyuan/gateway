/***********************************************************************************
Copy right:     Coffee Tech.
Author:         jiaoyue
Date:           2019.8.1
Description:    log测试
***********************************************************************************/

#include <log_utils.h>

#define MODULE "log_test"

int main(int argc, char *argv[])
{
    int i;
    unsigned char buf[10] = {0x12, 0x34, 0x56, 0x78, 0x90};

    int a = 5;

    set_dbg_level(APP_DEBUG);  //log直接送往当前终端时，通过代码设置打印级别

    //APP_DEBUG适用于不重要且一般不需要保留的调试信息
    LOG_STD(APP_DEBUG, MODULE, "a = %d", a);
    //APP_INFO适用于需要保留的调试信息
    LOG_STD(APP_INFO, MODULE, "a = %d", a);
    //APP_NOTICE适用于需要特别注意的调试信息
    LOG_STD(APP_NOTICE, MODULE, "a = %d", a);
    //APP_WARNING适用于出现问题但是不会影响程序正常运行的输出
    LOGW("a = %d", a);
    //APP_ERR适用于出现问题并且会导致出错的输出
    LOGE("a = %d", a);
    //APP_CRIT适用于会严重影响系统运行的状态输出
    LOG_STD(APP_CRIT, MODULE, "a = %d", a);
    //APP_ALERT适用于会严重影响系统运行的状态输出
    LOG_STD(APP_ALERT, MODULE, "a = %d", a);
    //APP_ALERT适用于会严重影响系统运行的状态输出
    LOG_STD(APP_EMERG, MODULE, "a = %d", a);

    //字节流输出案例
    LOG_STD(APP_DEBUG, MODULE, "buf len = %d", sizeof(buf));
    for(i=0; i<sizeof(buf); i++){
        LOG(APP_DEBUG, "%02X ", buf[i]);
    }
    LOG(APP_DEBUG, "\n");
}
