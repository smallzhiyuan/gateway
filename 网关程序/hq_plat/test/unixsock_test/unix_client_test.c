/***********************************************************************************
Copy right:     Coffee Tech.
Author:         jiaoyue
Date:           2019.10.31
Description:    unix sock测试
***********************************************************************************/

#include <unix_sock.h>
#include <log_utils.h>

static struct unix_param unix_para;

#define MODULE "unixc_test"

char buf[256];
char data[256];

int main(int argc, char *argv[])
{
    long i = 0;

    unix_init_client(&unix_para, MODULE, "unixs_test");
    while (1)
    {
        i++;
        sprintf(buf, "%ld", i);
        unix_client_send(&unix_para, buf, 256);

        unix_client_recv(&unix_para, data, 256, NULL);
        printf("recv from server = %s\n", data);
        sleep(1);
    }

    return 0;
}
