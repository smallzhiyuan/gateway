/***********************************************************************************
Copy right:     Coffee Tech.
Author:         jiaoyue
Date:           2019.10.31
Description:    unix sock测试
***********************************************************************************/

#include <unix_sock.h>
#include <log_utils.h>

static struct unix_param unix_para;

#define MODULE "unixs_test"

char data[256] = {0};

int main(int argc, char *argv[])
{
    unix_init_server(&unix_para, MODULE);
    while (1) 
    {
        if(unix_server_recv(&unix_para, data, 256) > 0)
        {
            printf("recv client data = %s\n", data);

            //收到后客户端地址会自动记录在unix_para中
            unix_server_send(&unix_para, data, 256);
        }
    }

    return 0;
}
