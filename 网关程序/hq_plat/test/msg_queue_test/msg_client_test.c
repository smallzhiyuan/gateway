/***********************************************************************************
Copy right:     Coffee Tech.
Author:         jiaoyue
Date:           2019.8.1
Description:    整体测试
***********************************************************************************/

#include <msg_queue.h>

static struct msg_param msg_para;

#define MODULE "msgc_test"

char buf[256];
char data[256];

int main(int argc, char *argv[])
{
    long i = 0;

    msg_client_init(&msg_para, MODULE, "msgs_test");
    while (1)
    {
        i++;
        sprintf(buf, "%ld", i);
        msg_para.msg_type = i;
        msg_client_send(&msg_para, buf, 256);

        msg_client_recv(&msg_para, data, 256);
        printf("recv from server = %s\n", data);
        sleep(1);
    }

    return 0;
}
