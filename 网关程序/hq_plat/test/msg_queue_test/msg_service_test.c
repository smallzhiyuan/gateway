/***********************************************************************************
Copy right:     Coffee Tech.
Author:         jiaoyue
Date:           2019.9.27
Description:    消息队列测试
***********************************************************************************/

#include <msg_queue.h>

static struct msg_param msg_para;
char data[256];
#define MODULE "msgs_test"

int main(int argc, char *argv[])
{
    msg_service_init(&msg_para, MODULE);
    while (1)
    {
        if(msg_service_recv(&msg_para, data, 256) > 0)
        {
            printf("recv client type = %ld, data = %s\n", msg_para.msg_type, data);

            //收到后客户端地址会自动记录在msg_para中
            msg_service_send(&msg_para, data, 256);
        }
    }

    return 0;
}
