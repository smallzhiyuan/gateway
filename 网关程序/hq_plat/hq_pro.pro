TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += include\
        src\
        utils

SOURCES += \
    app/demo/demo.c \
    utils/cJSON.c \
    utils/msg_queue.c \
    utils/msg_queue_peer.c \
    utils/shmem.c \
    utils/unix_sock.c \
    utils/cJSON.h \
    utils/list.h \
    utils/msg_queue.h \
    utils/msg_queue_peer.h \
    utils/service_pub.h \
    utils/shmem.h \
    utils/log_pub.h \
    utils/log_utils.h \
    utils/log_utils.c \
    utils/pub_define.h \
    utils/unix_sock.h \
    test/log_test/log_test.c \
    test/msg_queue_peer_test/msg_a.c \
    test/msg_queue_peer_test/msg_b.c \
    test/msg_queue_test/msg_client_test.c \
    test/msg_queue_test/msg_service_test.c \
    test/shm_test/shm_a.c \
    test/shm_test/shm_b.c \
    test/unixsock_test/unix_client_test.c \
    test/unixsock_test/unix_service_test.c

