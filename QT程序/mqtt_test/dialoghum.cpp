#include "dialoghum.h"
#include "ui_dialoghum.h"

DialogHum::DialogHum(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogHum)
{
    ui->setupUi(this);
    QCustomPlot *p=ui->widget;
    p->clearPlottables();
    p->setBackground(Qt::lightGray);

    p->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom); //可拖拽加滚轮缩放
    p->legend->setVisible(true); //右上角显示曲线缩略图
    p->xAxis->setLabel(QString("时间/s"));
    p->yAxis->setLabel(QString("湿度/%"));

    p->xAxis->setRange(0,24); //x轴数据范围
    p->yAxis->setRange(-10,20);//y轴数据范围

    p->addGraph();
    p->graph(0)->setPen(QPen(Qt::red));//设置折线颜色
    p->graph(0)->setName(QString("湿度")); //设置湿度折线
    p->xAxis->setLabelColor(Qt::red); //设置折线标签
    p->xAxis->setSubTickPen(QPen(Qt::red)); //设置刻度颜色
    p->yAxis->setLabelColor(Qt::red); //设置折线标签
    p->yAxis->setSubTickPen(QPen(Qt::red)); //设置刻度颜色


    for(int i=0;i<24;i++)
    {
        p->graph(0)->addData(i,i%4); //显示数据
    }
}

DialogHum::~DialogHum()
{
    delete ui;
}
