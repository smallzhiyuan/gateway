#ifndef DIALOGILL_H
#define DIALOGILL_H

#include <QDialog>
#include <QDebug>
#include <QColor>
#include "qcustomplot.h"

namespace Ui {
class DialogIll;
}

class DialogIll : public QDialog
{
    Q_OBJECT

public:
    explicit DialogIll(QWidget *parent = 0);
    ~DialogIll();

private:
    Ui::DialogIll *ui;
};

#endif // DIALOGILL_H
