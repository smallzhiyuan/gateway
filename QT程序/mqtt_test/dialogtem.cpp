#include "dialogtem.h"
#include "ui_dialogtem.h"

DialogTem::DialogTem(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogTem)
{
    ui->setupUi(this);

    QCustomPlot *p=ui->widget;
    p->clearPlottables();
    p->setBackground(Qt::lightGray);

    p->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom); //可拖拽加滚轮缩放
    p->legend->setVisible(true); //右上角显示曲线缩略图
    p->xAxis->setLabel(QString("时间/s"));
    p->yAxis->setLabel(QString("温度/°C"));

    p->xAxis->setRange(0,24); //x轴数据范围
    p->yAxis->setRange(-10,20);//y轴数据范围

    p->addGraph();
    p->graph(0)->setPen(QPen(Qt::blue));//设置折线颜色
    p->graph(0)->setName(QString("温度")); //设置温度折线
    p->xAxis->setLabelColor(Qt::blue); //设置折线标签
    p->xAxis->setSubTickPen(QPen(Qt::blue)); //设置刻度颜色
    p->yAxis->setLabelColor(Qt::blue); //设置折线标签
    p->yAxis->setSubTickPen(QPen(Qt::blue)); //设置刻度颜色


    for(int i=0;i<24;i++)
    {
        p->graph(0)->addData(i,i%4); //显示数据
    }

}

DialogTem::~DialogTem()
{
    delete ui;
}
