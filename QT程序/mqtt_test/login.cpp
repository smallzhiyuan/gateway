#include "login.h"
#include "ui_login.h"

Login::Login(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Login)
{
    ui->setupUi(this);
    connect(ui->pushButton_login,SIGNAL(clicked()),this,SLOT(myBtnLoginClicked()));
    connect(ui->pushButton_cancel,SIGNAL(clicked()),this,SLOT(myBtnCancelClicked()));

}

Login::~Login()
{
    delete ui;
}

void Login::keyPressEvent(QKeyEvent *event)
{
    if(event->key()==Qt::Key_Enter|| event->key() == Qt::Key_Return)
    {
        emit ui->pushButton_login->click();
    }
}

void Login::myBtnLoginClicked()
{
    if((ui->lineEdit_name->text()=="1")&&(ui->lineEdit_password->text()=="1"))
    {
        qDebug()<<"用户名" << ui->lineEdit_name->text();
        qDebug()<<"密码" << ui->lineEdit_password->text();
        this->hide();
        emit showMyMain();
    }
    else if((ui->lineEdit_name->text()=="")||(ui->lineEdit_password->text()==""))
    {
        QMessageBox::warning(this,"提示","用户名密码不能为空");
    }
    else if(ui->lineEdit_name->text()!="1")
    {
        QMessageBox::warning(this,"提示","用户名不存在，请重新输入");
        ui->lineEdit_name->clear();
        ui->lineEdit_password->clear();
        ui->lineEdit_name->setFocus();
    }
    else if((ui->lineEdit_name->text()=="1")&&(ui->lineEdit_password->text()!="1"))
    {
        QMessageBox::warning(this,"提示","用户名密码不匹配，请重新输入");
        ui->lineEdit_name->clear();
        ui->lineEdit_password->clear();
        ui->lineEdit_name->setFocus();
    }
}

void Login::myBtnCancelClicked()
{
    this->close();
}
