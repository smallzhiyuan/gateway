#ifndef WIDGET_H
#define WIDGET_H

#include <QtWidgets>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonParseError>
#include <QFile>
#include <QJsonObject>
#include "mqtt/qmqtt.h"
#include <QKeyEvent>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();
    int controlkey=0;
    void keyPressEvent(QKeyEvent *event);//键盘敲击事件
    int controllight=0;
    int controlalrm=0;
    int controlfan=0;


private slots:
    void doDataReceived(QMQTT::Message message);
    void doSubscribed(const QString& topic, const quint8 qos);
    void doConnected();  //MQTT 连接成功
    void btnSendClicked();//发送测试
    void loginSuccess();//登陆成功
    void btnQueryClicked();//查询槽
    void btnControlLightClicked();//控制灯
    void btnControlAlarmClicked();//控制烟雾报警
    void btnControlFanClicked();//控制风扇
    void btnPatternInitiativeClicked();//主动上报
    void btnPatternPeriodClicked();//周期上报
    void btnPatternChangeClicked();//变化上报

private:
    Ui::Widget *ui;
    QMQTT::Client *client;

};

#endif // WIDGET_H
