#ifndef DIALOGHUM_H
#define DIALOGHUM_H

#include <QDialog>
#include <QDebug>
#include <QColor>
#include "qcustomplot.h"

namespace Ui {
class DialogHum;
}

class DialogHum : public QDialog
{
    Q_OBJECT

public:
    explicit DialogHum(QWidget *parent = 0);
    ~DialogHum();

private:
    Ui::DialogHum *ui;
};

#endif // DIALOGHUM_H
