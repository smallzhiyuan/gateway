#ifndef LOGIN_H
#define LOGIN_H

#include <QWidget>
#include <QDebug>
#include <QMessageBox>
#include <QKeyEvent>
namespace Ui {
class Login;
}

class Login : public QWidget
{
    Q_OBJECT

public:
    explicit Login(QWidget *parent = 0);
    ~Login();
    void keyPressEvent(QKeyEvent *event);

private:
    Ui::Login *ui;
signals:
    void showMyMain();
private slots:
    void myBtnLoginClicked();
    void myBtnCancelClicked();

};

#endif // LOGIN_H
