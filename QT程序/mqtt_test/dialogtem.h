#ifndef DIALOGTEM_H
#define DIALOGTEM_H

#include <QDialog>
#include "qcustomplot.h"
#include <QDebug>
#include <QColor>


namespace Ui {
class DialogTem;
}

class DialogTem : public QDialog
{
    Q_OBJECT

public:
    explicit DialogTem(QWidget *parent = 0);
    ~DialogTem();

private:
    Ui::DialogTem *ui;
};

#endif // DIALOGTEM_H
