#include "widget.h"
#include "ui_widget.h"
#include "form1.h"
#include "form2.h"
#include "form3.h"
#include "dialogtem.h"
#include "dialoghum.h"
#include "dialogill.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    qDebug()<<"test";

    //初始化子页面
    Form1 *wig1=new Form1;
    Form2 *wig2=new Form2;
    Form3 *wig3=new Form3;
    //增加页面
    ui->stackedWidget->addWidget(wig1);
    ui->stackedWidget->addWidget(wig2);
    ui->stackedWidget->addWidget(wig3);
    //做信号连接
    QObject::connect(ui->listWidget, SIGNAL(currentRowChanged(int)),
                     ui->stackedWidget, SLOT(setCurrentIndex(int)));

    client = new QMQTT::Client(QHostAddress("192.168.31.242"),1883);
    client->setClientId("clientid");

    client->connectToHost();

    connect(ui->pushButton,SIGNAL(clicked()),this,SLOT(btnSendClicked()));//

    connect(client,SIGNAL(connected()),this,SLOT(doConnected()));

    connect(client,SIGNAL(subscribed(const QString&, const quint8)),\
            this,SLOT(doSubscribed(const QString&, const quint8)));

    connect(client,SIGNAL(received(QMQTT::Message)),\
            this,SLOT(doDataReceived(QMQTT::Message)));

    //按钮背景
    ui->toolButton_tem->setIcon(QIcon(":/new/prefix1/p8.png"));
    ui->toolButton_tem->setIconSize(QSize(100,100));
    ui->toolButton_hum->setIcon(QIcon(":/new/prefix1/p7.png"));
    ui->toolButton_hum->setIconSize(QSize(100,100));
    ui->toolButton_ill->setIcon(QIcon(":/new/prefix1/p3.png"));
    ui->toolButton_ill->setIconSize(QSize(100,100));

    ui->toolButton_light->setIcon(QIcon(":/new/prefix1/p1.png"));
    ui->toolButton_light->setIconSize(QSize(100,100));
    ui->toolButton_alarm->setIcon(QIcon(":/new/prefix1/p9.png"));
    ui->toolButton_alarm->setIconSize(QSize(100,100));
    ui->toolButton_fan->setIcon(QIcon(":/new/prefix1/p2.png"));
    ui->toolButton_fan->setIconSize(QSize(100,100));

    ui->toolButton_rain->setIcon(QIcon(":/new/prefix1/p4.png"));
    ui->toolButton_rain->setIconSize(QSize(100,100));
    ui->toolButton_oven->setIcon(QIcon(":/new/prefix1/p8.png"));
    ui->toolButton_oven->setIconSize(QSize(100,100));
    ui->toolButton_lock->setIcon(QIcon(":/new/prefix1/p6.png"));
    ui->toolButton_lock->setIconSize(QSize(100,100));

}

void Widget::doConnected()
{
    qDebug()<<"doConnected";
    client->subscribe("/app/data/up");
    client->subscribe("/app/control/up");
    client->subscribe("/app/mode/up");
}

void Widget::btnSendClicked()
{

    //    client->connectToHost();
    //    client->setWillTopic("down");
    //    client->setWillMessage("3333");
    //    client->disconnectFromHost();
    //    client->connectToHost();

    QMQTT::Message msg;
    msg.setTopic("/app/data/down");
    QString str="2222222";
    msg.setPayload(str.toLocal8Bit());
    client->publish(msg);

    //打开文件
    QFile loadFile(":/new/prefix1/node.json");
    if(!loadFile.open(QIODevice::ReadOnly))
    {
        qDebug()<<"could not open json";
        return;
    }
    QByteArray allData=loadFile.readAll(); //存放 读取数据
    loadFile.close();

    QJsonParseError jsonError;
    QJsonDocument jsonDoc(QJsonDocument::fromJson(allData,&jsonError));

    if(jsonError.error!=QJsonParseError::NoError)
    {
        qDebug() << "json error!";
        return;
    }

    QJsonObject rootObj=jsonDoc.object();

    QStringList list=rootObj.keys();
    //遍历jsonkey
    for(int i=0;i<list.count();i++)
    {
        qDebug() << "key" << i << " is:" << list.at(i);
    }

    qDebug() << rootObj["version"].toString();
    QString s=rootObj["version"].toString();
    QString s2="aaaa";
    qDebug() << "sad" << s<< s2;

    //解析json.node地址
    QJsonObject mqttserver=rootObj["mqtt_server"].toObject();
    qDebug() << mqttserver["addr"].toString();
    qDebug() << mqttserver["port"].toInt();

    //解析json.node数组
    QJsonObject mo=rootObj["m0"].toObject();
    QJsonArray m0Array=mo.value("data").toArray();
    for(int i =0;i<m0Array.size();i++)
    {
        QJsonObject array=m0Array.at(i).toObject();
        qDebug()<< array["key"].toInt();
    }
}

void Widget::loginSuccess()
{
    this->show();

    //query
    connect(ui->toolButton_tem,SIGNAL(clicked()),this,SLOT(btnQueryClicked()));
    connect(ui->toolButton_hum,SIGNAL(clicked()),this,SLOT(btnQueryClicked()));
    connect(ui->toolButton_ill,SIGNAL(clicked()),this,SLOT(btnQueryClicked()));
    //control
    connect(ui->toolButton_light,SIGNAL(clicked()),this,SLOT(btnControlLightClicked()));
    connect(ui->toolButton_alarm,SIGNAL(clicked()),this,SLOT(btnControlAlarmClicked()));
    connect(ui->toolButton_fan,SIGNAL(clicked()),this,SLOT(btnControlFanClicked()));
    //mode
    connect(ui->pushButton_initiative,SIGNAL(clicked()),this,SLOT(btnPatternInitiativeClicked()));
    connect(ui->pushButton_period,SIGNAL(clicked()),this,SLOT(btnPatternPeriodClicked()));
    connect(ui->pushButton_change,SIGNAL(clicked()),this,SLOT(btnPatternChangeClicked()));
}

void Widget::btnQueryClicked()
{
    //构建json数组
    QJsonArray msgkeys;
    msgkeys.append(1);
    msgkeys.append(2);
    msgkeys.append(3);
    msgkeys.append(4);
    //构建json对象
    QJsonObject msgg;
    msgg.insert("keys",QJsonValue(msgkeys));
    msgg.insert("limit","all");
    msgg.insert("type",1);

    //构建json文档
    QJsonDocument msgdocument;
    msgdocument.setObject(msgg);
    QByteArray msgbyte=msgdocument.toJson(QJsonDocument::Compact);
    QString msgstr(msgbyte);
    qDebug() << msgstr;

    //发送json
    QMQTT::Message msg1;
    msg1.setTopic("/app/data/down");
    msg1.setPayload(msgstr.toLocal8Bit());
    client->publish(msg1);
}

void Widget::btnControlLightClicked()
{
    int val=0;
    if(++controllight%2==1)
    {
        val=1;
        QString qstrStylesheet1 = "background-color:rgb(100,100,100)";
        ui->toolButton_light->setStyleSheet(qstrStylesheet1);
    }
    else
    {
        val=0;
        QString qstrStylesheet1 = "background-color:rgb(255,255,255)";
        ui->toolButton_light->setStyleSheet(qstrStylesheet1);
    }
    QString strval;
    strval.setNum(val);
    qDebug()<<"val "<< strval;

    //构建控制json语句对象
    QJsonObject lightdata;
    lightdata.insert("key",4);
    lightdata.insert("val",strval);
    QJsonObject controllight;
    controllight.insert("type",2);
    controllight.insert("data",lightdata);

    //构建控制json文档
    QJsonDocument lightdocument;
    lightdocument.setObject(controllight);
    QByteArray lightbyte=lightdocument.toJson(QJsonDocument::Compact);
    QString lightstr(lightbyte);
    qDebug() << lightstr;

    //发送控制json
    QMQTT::Message lightmsg;
    lightmsg.setTopic("/app/control/down");
    lightmsg.setPayload(lightstr.toLocal8Bit());
    client->publish(lightmsg);

    //设置标志位哪个key被按下
    controlkey=4;

//    QString qstrStylesheet1 = "background-color:rgb(255,255,255)";
//    ui->toolButton_light->setStyleSheet(qstrStylesheet1);
}

void Widget::btnControlAlarmClicked()
{ 
    int val=0;
    if(++controlalrm%2==1)
    {
        val=1;
        QString qstrStylesheet2 = "background-color:rgb(100,100,100)";
        ui->toolButton_alarm->setStyleSheet(qstrStylesheet2);
    }
    else
    {
        val=0;
        QString qstrStylesheet2 = "background-color:rgb(255,255,255)";
        ui->toolButton_alarm->setStyleSheet(qstrStylesheet2);
    }
    QString strval;
    strval.setNum(val);
    qDebug()<<"val "<< strval;

    //构建控制json语句对象
    QJsonObject alarmdata;
    alarmdata.insert("key",5);
    alarmdata.insert("val",strval);
    QJsonObject controlalarm;
    controlalarm.insert("type",2);
    controlalarm.insert("data",alarmdata);

    //构建控制json文档
    QJsonDocument alarmdocument;
    alarmdocument.setObject(controlalarm);
    QByteArray alarmbyte=alarmdocument.toJson(QJsonDocument::Compact);
    QString alarmstr(alarmbyte);
    qDebug() << alarmstr;

    //发送控制json
    QMQTT::Message alarmmsg;
    alarmmsg.setTopic("/app/control/down");
    alarmmsg.setPayload(alarmstr.toLocal8Bit());
    client->publish(alarmmsg);

    //设置标志位哪个key被按下
    controlkey=5;

//    QString qstrStylesheet2 = "background-color:rgb(255,255,255)";
//    ui->toolButton_alarm->setStyleSheet(qstrStylesheet2);
}

void Widget::btnControlFanClicked()
{
    int val=0;
    if(++controlalrm%2==1)
    {
        val=1;
        QString qstrStylesheet3 = "background-color:rgb(100,100,100)";
        ui->toolButton_fan->setStyleSheet(qstrStylesheet3);
    }
    else
    {
        val=0;
        QString qstrStylesheet3 = "background-color:rgb(255,255,255)";
        ui->toolButton_fan->setStyleSheet(qstrStylesheet3);
    }
    QString strval;
    strval.setNum(val);
    qDebug()<<"val "<< strval;

    //构建控制json语句对象
    QJsonObject fandata;
    fandata.insert("key",6);
    fandata.insert("val",strval);
    QJsonObject controlfan;
    controlfan.insert("type",2);
    controlfan.insert("data",fandata);

    //构建控制json文档
    QJsonDocument fandocument;
    fandocument.setObject(controlfan);
    QByteArray fanbyte=fandocument.toJson(QJsonDocument::Compact);
    QString fanstr(fanbyte);
    qDebug() << fanstr;

    //发送控制json
    QMQTT::Message fanmsg;
    fanmsg.setTopic("/app/control/down");
    fanmsg.setPayload(fanstr.toLocal8Bit());
    client->publish(fanmsg);

    //设置标志位哪个key被按下
    controlkey=6;

//    QString qstrStylesheet3 = "background-color:rgb(255,255,255)";
//    ui->toolButton_fan->setStyleSheet(qstrStylesheet3);

}

void Widget::btnPatternInitiativeClicked()
{
    //设置按钮颜色
    QString qstrStylesheet1 = "background-color:rgb(100,100,100)";
    ui->pushButton_initiative->setStyleSheet(qstrStylesheet1);
    QString qstrStylesheet2 = "background-color:rgb(255,255,255)";
    ui->pushButton_change->setStyleSheet(qstrStylesheet2);
    QString qstrStylesheet3 = "background-color:rgb(255,255,255)";
    ui->pushButton_period->setStyleSheet(qstrStylesheet3);

    //构建控制json语句对象
    QJsonObject modedata;
    modedata.insert("type",0);
    modedata.insert("period",5);
    QJsonObject mode;
    mode.insert("data",modedata);

    //构建json文档
    QJsonDocument initiativemode;
    initiativemode.setObject(mode);
    QByteArray initiativebyte=initiativemode.toJson(QJsonDocument::Compact);
    QString initiativestr(initiativebyte);
    qDebug()<<initiativestr;

    //发送修改模式json
    QMQTT::Message initiativemsg;
    initiativemsg.setTopic("/app/mode/down");
    initiativemsg.setPayload(initiativestr.toLocal8Bit());
    client->publish(initiativemsg);

}

void Widget::btnPatternPeriodClicked()
{
    //设置按钮颜色
    QString qstrStylesheet1 = "background-color:rgb(255,255,255)";
    ui->pushButton_initiative->setStyleSheet(qstrStylesheet1);
    QString qstrStylesheet2 = "background-color:rgb(255,255,255)";
    ui->pushButton_change->setStyleSheet(qstrStylesheet2);
    QString qstrStylesheet3 = "background-color:rgb(100,100,100)";
    ui->pushButton_period->setStyleSheet(qstrStylesheet3);

    //获得设置的period
    int perioddata=5;
    QString period=ui->lineEdit_period->text();
    if(ui->lineEdit_period->text()!="")
    {
        perioddata=period.toInt();
    }

    //构建控制json语句对象
    QJsonObject modedata;
    modedata.insert("type",2);
    modedata.insert("period",perioddata);
    QJsonObject mode;
    mode.insert("data",modedata);

    //构建json文档
    QJsonDocument periodmode;
    periodmode.setObject(mode);
    QByteArray periodbyte=periodmode.toJson(QJsonDocument::Compact);
    QString periodstr(periodbyte);
    qDebug()<<periodstr;

    //发送修改模式json
    QMQTT::Message periodmsg;
    periodmsg.setTopic("/app/mode/down");
    periodmsg.setPayload(periodstr.toLocal8Bit());
    client->publish(periodmsg);
}

void Widget::btnPatternChangeClicked()
{
    //设置按钮颜色
    QString qstrStylesheet1 = "background-color:rgb(255,255,255)";
    ui->pushButton_initiative->setStyleSheet(qstrStylesheet1);
    QString qstrStylesheet2 = "background-color:rgb(100,100,100)";
    ui->pushButton_change->setStyleSheet(qstrStylesheet2);
    QString qstrStylesheet3 = "background-color:rgb(255,255,255)";
    ui->pushButton_period->setStyleSheet(qstrStylesheet3);

    //构建控制json语句对象
    QJsonObject modedata;
    modedata.insert("type",1);
    modedata.insert("period",5);
    QJsonObject mode;
    mode.insert("data",modedata);

    //构建json文档
    QJsonDocument changemode;
    changemode.setObject(mode);
    QByteArray changebyte=changemode.toJson(QJsonDocument::Compact);
    QString changestr(changebyte);
    qDebug()<<changestr;

    //发送修改模式json
    QMQTT::Message changemsg;
    changemsg.setTopic("/app/mode/down");
    changemsg.setPayload(changestr.toLocal8Bit());
    client->publish(changemsg);
}

void Widget::doDataReceived(QMQTT::Message message)
{
    QString mes = QString(message.id())+" "+QString(message.qos())+" "+message.topic()+" "+message.payload()+"\n";
    QString str = message.payload();
    qDebug()<< "mes="<<str;
    //    qDebug()<< mes << 1 ;

    //解析json
    QByteArray report=str.toLocal8Bit();
    QJsonParseError jsonError;
    QJsonDocument reportDoc(QJsonDocument::fromJson(report,&jsonError));


    QJsonObject reportObj=reportDoc.object();

    //        qDebug() << "all type "<<reportObj["type"].toInt();
    //        qDebug() <<reportObj["type"].toInt();
    //        qDebug() <<reportObj["type"].toString();

    //        qDebug() <<reportObj["result"].toInt();
    //        qDebug() <<reportObj["msg"].toString();

    qDebug()<< "report type  "<<reportObj["type"].toInt();
    //判断接收消息类型
    if(reportObj["type"].toInt()==1)
    {
        qDebug() << reportObj["result"].toInt();

        //解析json数组
        // QJsonObject reportdata=reportObj["data"].toObject();
        QJsonArray dataArray=reportObj.value("data").toArray();
        for(int i =0;i<dataArray.size();i++)
        {
            QJsonObject arraykey=dataArray.at(i).toObject();
            //            qDebug()<< arraykey["key"].toInt();
            //            qDebug()<< arraykey["val"].toString();
            if(arraykey["key"].toInt()==1)
            {
                qDebug() << arraykey["val"].toString();
                ui->toolButton_tem->setText(arraykey["val"].toString().append("°C"));
            }
            else if(arraykey["key"].toInt()==2)
            {
                qDebug() << arraykey["val"].toString();
                ui->toolButton_hum->setText(arraykey["val"].toString().append("%"));
            }
            else if(arraykey["key"].toInt()==3)
            {
                qDebug() << arraykey["val"].toString();
                ui->toolButton_ill->setText(arraykey["val"].toString().append(" Lux"));
            }
        }
    }
    else if(reportObj["type"].toInt()==2)
    {
        //        qDebug() <<"type "<<reportObj["type"].toInt();
        //        qDebug() <<"resultrfv "<<reportObj["result"].toInt();
        //        qDebug() <<"msg "<<reportObj["msg"].toString();

        //判断控制结构是否成功
        if(reportObj["result"].toInt()==0)
        {
            if(controlkey==4)
            {
                QString qstrStylesheet1 = "background-color:rgb(100,100,100)";
                ui->toolButton_light->setStyleSheet(qstrStylesheet1);
            }
            else if(controlkey==5)
            {
                QString qstrStylesheet2 = "background-color:rgb(100,100,100)";
                ui->toolButton_alarm->setStyleSheet(qstrStylesheet2);
            }
            else if(controlkey==6)
            {
                QString qstrStylesheet3 = "background-color:rgb(100,100,100)";
                ui->toolButton_fan->setStyleSheet(qstrStylesheet3);
            }
        }
    }
    else
    {
        qDebug() <<"result ："<<reportObj["result"].toInt();
        qDebug() <<"msg ："<<reportObj["msg"].toString();
        if(reportObj["result"].toInt()==0)
        {
            ui->label_report->setText("修改成功！");
        }
        else
        {
            ui->label_report->setText("修改失败！");
        }
    }
}

void Widget::doSubscribed(const QString& topic, const quint8 qos)
{
    qDebug()<<"doSubscribed = "<<topic;
}


void Widget::keyPressEvent(QKeyEvent *event)
{
    qDebug() << "clicked";
    if(event->key()==Qt::Key_1)
    {
        DialogTem *dt;
        dt=new DialogTem(this);
        dt->show();
    }
    else if(event->key()==Qt::Key_2)
    {
        DialogHum *dh;
        dh=new DialogHum(this);
        dh->show();
    }
    else if(event->key()==Qt::Key_3)
    {
        DialogIll *di;
        di=new DialogIll(this);
        di->show();
    }
}

Widget::~Widget()
{
    delete ui;
}


