#-------------------------------------------------
#
# Project created by QtCreator 2021-11-22T11:28:33
#
#-------------------------------------------------

QT       += core gui network
QT       += printsupport
QMAKE_CXXFLAGS += -std=c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = untitled
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    form1.cpp \
    form2.cpp \
    form3.cpp \
    login.cpp \
    dialogtem.cpp \
    dialoghum.cpp \
    dialogill.cpp \
    qcustomplot.cpp

HEADERS  += widget.h \
    form1.h \
    form2.h \
    form3.h \
    login.h \
    dialogtem.h \
    dialoghum.h \
    dialogill.h \
    qcustomplot.h

FORMS    += widget.ui \
    form1.ui \
    form2.ui \
    form3.ui \
    login.ui \
    dialogtem.ui \
    dialoghum.ui \
    dialogill.ui

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/lib/ -lQt5Qmqtt
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/lib/ -lQt5Qmqttd

INCLUDEPATH += $$PWD/.
INCLUDEPATH += $$PWD/mqtt
DEPENDPATH += $$PWD/.

RESOURCES += \
    res.qrc
