#include "form1.h"
#include "ui_form1.h"

Form1::Form1(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form1)
{
    ui->setupUi(this);
    qDebug()<<"test";

    client = new QMQTT::Client(QHostAddress("192.168.31.242"),1883);
    client->setClientId("clientid2");

    client->connectToHost();

    connect(client,SIGNAL(connected()),this,SLOT(doConnected()));

    connect(client,SIGNAL(subscribed(const QString&, const quint8)),\
            this,SLOT(doSubscribed(const QString&, const quint8)));

    connect(client,SIGNAL(received(QMQTT::Message)),\
            this,SLOT(doDataReceived(QMQTT::Message)));

    //建立modbus查询指令
    connect(ui->pushButton_tem,SIGNAL(clicked()),this,SLOT(btnModbusQueryClicked()));
    connect(ui->pushButton_hum,SIGNAL(clicked()),this,SLOT(btnModbusQueryClicked()));
    connect(ui->pushButton_shuaxin,SIGNAL(clicked()),this,SLOT(btnModbusQueryClicked()));
    //建立控制modbus指令
    connect(ui->pushButton_switch,SIGNAL(clicked()),this,SLOT(btnModbusAirswitchClicked()));
    connect(ui->pushButton_airtemp,SIGNAL(clicked()),this,SLOT(btnModbusAirtemClicked()));
    connect(ui->pushButton_light,SIGNAL(clicked()),this,SLOT(btnModbuslightClicked()));

    //设置按钮背景
    ui->pushButton_tem->setIcon(QIcon(":/new/prefix1/mp1.png"));
    ui->pushButton_tem->setIconSize(QSize(100,100));
    ui->pushButton_hum->setIcon(QIcon(":/new/prefix1/mp2.png"));
    ui->pushButton_hum->setIconSize(QSize(100,100));

    ui->pushButton_airtemp->setIcon(QIcon(":/new/prefix1/mp4.png"));
    ui->pushButton_airtemp->setIconSize(QSize(100,100));
    ui->pushButton_switch->setIcon(QIcon(":/new/prefix1/mp3.png"));
    ui->pushButton_switch->setIconSize(QSize(100,100));
    ui->pushButton_light->setIcon(QIcon(":/new/prefix1/mp5.png"));
    ui->pushButton_light->setIconSize(QSize(100,100));


}

Form1::~Form1()
{
    delete ui;
}

void Form1::doDataReceived(QMQTT::Message message)
{
    QString mes = QString(message.id())+" "+QString(message.qos())+" "+message.topic()+" "+message.payload()+"\n";
    QString str = message.payload();
    qDebug()<<"mdobus mes="<<str;

    //解析json
    QByteArray report=str.toLocal8Bit();
    QJsonParseError jsonError;
    QJsonDocument reportDoc(QJsonDocument::fromJson(report,&jsonError));


    QJsonObject reportObj=reportDoc.object();

    //判断接收消息类型
    if(reportObj["type"].toInt()==1)
    {
        qDebug() << reportObj["result"].toInt();
        qDebug() << 1;

        //解析json数组
        // QJsonObject reportdata=reportObj["data"].toObject();
        QJsonArray dataArray=reportObj.value("data").toArray();
        for(int i =0;i<dataArray.size();i++)
        {
            QJsonObject arraykey=dataArray.at(i).toObject();
            //                        qDebug()<< arraykey["key"].toInt();
            //                        qDebug()<< arraykey["val"].toString();
            if(arraykey["key"].toInt()==101)
            {
                qDebug() << arraykey["val"].toString();
                ui->pushButton_tem->setText(arraykey["val"].toString().append("°C"));
            }
            else if(arraykey["key"].toInt()==102)
            {
                qDebug() << arraykey["val"].toString();
                ui->pushButton_hum->setText(arraykey["val"].toString().append("%"));
            }
        }
    }
}

void Form1::doSubscribed(const QString &topic, const quint8 qos)
{
    qDebug()<< "modbus  "<<"doSubscribed = "<<topic;
}

void Form1::doConnected()
{
    qDebug()<<"doConnected";
    client->subscribe("/app/data/up");
}

void Form1::btnModbusQueryClicked()
{
    //构建json数组
    QJsonArray msgkeys;
    msgkeys.append(1);
    msgkeys.append(2);
    msgkeys.append(3);
    msgkeys.append(4);
    //构建json对象
    QJsonObject msgg;
    msgg.insert("keys",QJsonValue(msgkeys));
    msgg.insert("limit","all");
    msgg.insert("type",1);

    //构建json文档
    QJsonDocument msgdocument;
    msgdocument.setObject(msgg);
    QByteArray msgbyte=msgdocument.toJson(QJsonDocument::Compact);
    QString msgstr(msgbyte);
    qDebug() << msgstr;

    //发送json
    QMQTT::Message msg1;
    msg1.setTopic("/app/data/down");
    msg1.setPayload(msgstr.toLocal8Bit());
    client->publish(msg1);

}

void Form1::btnModbuslightClicked()
{
    //构造控制开关标志
    int val=0;
    if(++light%2==0)
    {
        QString qstrStylesheet1 = "background-color:rgb(100,100,100)";
        ui->pushButton_light->setStyleSheet(qstrStylesheet1);
        val=1;
    }
    else
    {
        QString qstrStylesheet1 = "background-color:rgb(255,255,255)";
        ui->pushButton_light->setStyleSheet(qstrStylesheet1);
        val=0;
    }
    QString strval;
    strval.setNum(val);
    qDebug() << strval;

    //构建控制json语句对象
    QJsonObject Mlightdata;
    Mlightdata.insert("key",105);
    Mlightdata.insert("val",strval);
    QJsonObject controlMlight;
    controlMlight.insert("type",2);
    controlMlight.insert("data",Mlightdata);

    //构建控制json文档
    QJsonDocument Mlightdocument;
    Mlightdocument.setObject(controlMlight);
    QByteArray Mlightbyte=Mlightdocument.toJson(QJsonDocument::Compact);
    QString Mlightstr(Mlightbyte);
    qDebug() << Mlightstr;

    //发送控制json
    QMQTT::Message Mlightmsg;
    Mlightmsg.setTopic("/app/control/down");
    Mlightmsg.setPayload(Mlightstr.toLocal8Bit());
    client->publish(Mlightmsg);
}

void Form1::btnModbusAirswitchClicked()
{
    //构造控制开关标志
    int val=0;
    if(++airswitch%2==1)
    {
        QString qstrStylesheet1 = "background-color:rgb(100,100,100)";
        ui->pushButton_switch->setStyleSheet(qstrStylesheet1);
        val=1;
    }
    else
    {
        QString qstrStylesheet1 = "background-color:rgb(255,255,255)";
        ui->pushButton_switch->setStyleSheet(qstrStylesheet1);
        val=0;
    }
    QString strval;
    strval.setNum(val);
    qDebug() << strval;

    //构建控制json语句对象
    QJsonObject switchdata;
    switchdata.insert("key",103);
    switchdata.insert("val",strval);
    QJsonObject controlswitch;
    controlswitch.insert("type",2);
    controlswitch.insert("data",switchdata);

    //构建控制json文档
    QJsonDocument switchdocument;
    switchdocument.setObject(controlswitch);
    QByteArray switchbyte=switchdocument.toJson(QJsonDocument::Compact);
    QString switchstr(switchbyte);
    qDebug() << switchstr;

    //发送控制json
    QMQTT::Message switchmsg;
    switchmsg.setTopic("/app/control/down");
    switchmsg.setPayload(switchstr.toLocal8Bit());
    client->publish(switchmsg);

}

void Form1::btnModbusAirtemClicked()
{
    //构造控制开关标志
    int val=0;
    if(++airtemp%2==0)
    {
        QString qstrStylesheet1 = "background-color:rgb(100,100,100)";
        ui->pushButton_airtemp->setStyleSheet(qstrStylesheet1);
        val=1;
    }
    else
    {
        QString qstrStylesheet1 = "background-color:rgb(255,255,255)";
        ui->pushButton_airtemp->setStyleSheet(qstrStylesheet1);
        val=0;
    }
    QString strval;
    strval.setNum(val);
    qDebug() << strval;

    //构建控制json语句对象
    QJsonObject airtempdata;
    airtempdata.insert("key",104);
    airtempdata.insert("val",strval);
    QJsonObject controlairtemp;
    controlairtemp.insert("type",2);
    controlairtemp.insert("data",airtempdata);

    //构建控制json文档
    QJsonDocument airtempdocument;
    airtempdocument.setObject(controlairtemp);
    QByteArray airtempbyte=airtempdocument.toJson(QJsonDocument::Compact);
    QString airtempstr(airtempbyte);
    qDebug() << airtempstr;

    //发送控制json
    QMQTT::Message airtempmsg;
    airtempmsg.setTopic("/app/control/down");
    airtempmsg.setPayload(airtempstr.toLocal8Bit());
    client->publish(airtempmsg);
}
