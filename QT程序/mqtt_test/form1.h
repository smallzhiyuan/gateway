#ifndef FORM1_H
#define FORM1_H

#include <QtWidgets>
#include "mqtt/qmqtt.h"

namespace Ui {
class Form1;
}

class Form1 : public QWidget
{
    Q_OBJECT

public:
    explicit Form1(QWidget *parent = 0);
    ~Form1();
    int airswitch=0;
    int airtemp=1;
    int light=1;
private slots:
    void doDataReceived(QMQTT::Message message);
    void doSubscribed(const QString& topic, const quint8 qos);
    void doConnected();  //MQTT 连接成功

    void btnModbusQueryClicked();//查询指令

    void btnModbuslightClicked();//控制灯
    void btnModbusAirswitchClicked();//控制空调开关
    void btnModbusAirtemClicked();//控制保持空调温度
private:
    Ui::Form1 *ui;
    QMQTT::Client *client;
};

#endif // FORM1_H
