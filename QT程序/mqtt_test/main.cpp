#include "widget.h"
#include <QApplication>
#include "login.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Login log;
//    log.setWindowTitle("登陆界面");
    log.setWindowFlags(Qt::FramelessWindowHint);

    log.show();

    Widget w;
    w.setWindowTitle("主控界面");
//    w.show();

    QObject::connect(&log,SIGNAL(showMyMain()),&w,SLOT(loginSuccess()));


    return a.exec();
}
